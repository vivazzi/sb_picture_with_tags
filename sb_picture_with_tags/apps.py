from django.apps import AppConfig

from django.utils.translation import gettext_lazy as _


class SBPictureWithTagsConfig(AppConfig):
    name = 'sb_picture_with_tags'
    verbose_name = _('Picture (with tags)')
