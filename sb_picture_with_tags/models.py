from os.path import basename
from taggit.managers import TaggableManager

from sb_core.cms_models import SBCMSPluginMixin
from sb_picture.models import SBPicture


class SBPictureWithTags(SBPicture):
    tags = TaggableManager(blank=True)

    def copy_relations(self, old_instance):
        self.tags.clear()
        for tag in old_instance.tags.all():
            self.tags.add(tag.name)

        SBCMSPluginMixin.copy_relations(self, old_instance)

    def __str__(self):
        return self.hint or basename(self.pic.name)

    class Meta:
        db_table = 'sb_picture_with_tags'
        verbose_name = 'Картинка с тегами'
        verbose_name_plural = 'Картинки с тегами'
