from django.db import models, migrations
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('sb_picture', '0001_initial'),
        ('taggit', '0002_auto_20150616_2121'),
    ]

    operations = [
        migrations.CreateModel(
            name='SBPictureWithTags',
            fields=[
                ('sbpicture_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sb_picture.SBPicture', on_delete=models.CASCADE)),
                ('tags', taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text='A comma-separated list of tags.', verbose_name='Tags')),
            ],
            options={
                'db_table': 'sb_picture_with_tags',
                'verbose_name': '\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430 \u0441 \u0442\u0435\u0433\u0430\u043c\u0438',
                'verbose_name_plural': '\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0438 \u0441 \u0442\u0435\u0433\u0430\u043c\u0438',
            },
            bases=('sb_picture.sbpicture',),
        ),
    ]
