from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from sb_core.constants import BASE
from sb_core.utils.thumbnail import thumb
from sb_picture.forms import SBPictureForm
from sb_picture_with_tags.models import SBPictureWithTags


class SBPictureWithTagsPlugin(CMSPluginBase):
    module = BASE
    model = SBPictureWithTags
    name = 'Картинка с тегами'
    form = SBPictureForm
    render_template = 'sb_picture/sb_picture.html'
    text_enabled = True

    fieldsets = (
        ('', {
            'fields': ('pic', 'hint',
                       ('alignment', 'float'),
                       ('width', 'height'),
                       'is_full_display',)
        }),
        ('Внешний вид картинки', {
            'fields': (('is_shadow', 'shadow_pars'),
                       ('is_border', 'border_size', 'border_color'))
        }),
        ('Метки', {
            'fields': ('tags', )
        }),
        ('Дополнительные настройки', {
            'classes': ('collapse',),
            'fields': ('attr_class', )
        }),
    )

    def render(self, context, instance, placeholder):
        context = super(SBPictureWithTagsPlugin, self).render(context, instance, placeholder)

        context['is_full_display'] = instance.is_full_display

        parent = instance.parent
        if parent and parent.get_plugin_instance() and hasattr(parent.get_plugin_instance()[0], 'plugin_type') and \
                        parent.get_plugin_instance()[0].plugin_type == 'SBLinkPlugin':
            context['is_full_display'] = False
        return context


plugin_pool.register_plugin(SBPictureWithTagsPlugin)
